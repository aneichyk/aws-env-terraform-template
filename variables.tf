variable "profile_name" {
  type        = string
  description = "Profile name being used."
}

variable "region" {
  type        = string
  description = "Available AWS regions."
}

variable "account_id" {
  type        = string
  description = "AWS account with this infrastructure."
}

