# AWS infrastructure for CLIENT

## Description

This repository provisions the AWS infrastructure for CLIENT

Terraform version: 0.13.4

We assume the AWS root account is created and has IAM user with programmic access.

Before initiating terraform modify the following:
 
*   Add your AWS credentials to `~/.aws/credentials` and
`~/.aws/config` as separate profile CLIENT 
*   Create a key-pair in EC2 service `<KEY.PAIR>` (might be worth 
implementing key creation in terraform).
*   Change the account name, AWS account ID, and region in `terraform.tfvars` 
*   Change your credentials profile (CLIENT) and terraform bucket name in `setup-tf-bucket.sh`
*   Change profile and terraform bucket name in `main.tf`
*   Change the `KEY.PAIR` to yours in `instances.tf` and `compute-env/ami.tf` 

## Initiating infrastructure


1. Set-up terraform bucket where terraform state will be stored

```
. setup-tf-bucket.sh
```

2. Initialize terraform:

```
terraform init
```


## Apply new changes to infrastructure

1. Check for the changes to be applied
```
$ terraform plan
```

2. If the check is consistent with expected changes, apply the changes:
```
$ terraform apply
```


Upon first creation of Compute environment, EC2 instance will be created and 
started. Make sure to login to AWS console, go to EC2 - Instances and Stop the 
running instance, but do not terminate it! 

